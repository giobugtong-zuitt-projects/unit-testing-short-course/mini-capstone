# Capstone

---

## Objectives
1. Create a test suite given an application
2. Create a function using the TDD process

---
Capstone:
Given the backend boiler plate, create a test suite AND the post functionality for /currency using the TDD and the following test scenarios:

1. Check if post /currency is running																		done
2. Check if post /currency returns status 400 if name is missing											
3. Check if post /currency returns status 400 if name is not a string										
4. Check if post /currency returns status 400 if name is empty												
5. Check if post /currency returns status 400 if ex is missing												
6. Check if post /currency returns status 400 if ex is not an object										
7. Check if post /currency returns status 400 if ex is empty												
8. Check if post /currency returns status 400 if alias is missing											
9. Check if post /currency returns status 400 if alias is not an string										
10. Check if post /currency returns status 400 if alias is empty											
11. Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias	
12. Check if post /currency returns status 200 if all fields are complete and there are no duplicates		

Format of data to be sent:
```javascript
{
	'alias': 'riyadh',
	'name': 'Saudi Arabian Riyadh',
	'ex': {
		'peso': 0.47,
        'usd': 0.0092,
        'won': 10.93,
        'yuan': 0.065
	}
}

```