const { assert } = require('chai');
const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', (done) => {
	//add the solutions here
	const domain = "http://localhost:5000"

	// POST /currency is running
	it("POST /currency", (done) => {
		chai.request(domain)
		.post("/currency")
		.send({})
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();
		})
	})

	//POST /currency 400 - property 'name' is missing
	it("POST /currency - property 'name' is missing", (done) => {
		chai.request(domain)
		.post("/currency")
		.type("json")
		.send({
			'alias': 'riyadh',
			'ex': {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	//POST /currency 400 - property 'name' is NOT a string
	it("POST /currency - property 'name' is NOT a string", (done) => {
		chai.request(domain)
		.post("/currency")
		.type("json")
		.send({
			'alias': 'riyadh',
			'name': 69,
			'ex': {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	//POST /currency 400 - property 'name' is empty
	it("POST /currency - property 'name' is empty", (done) => {
		chai.request(domain)
		.post("/currency")
		.type("json")
		.send({
			'alias': 'riyadh',
			'name': '',
			'ex': {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	//POST /currency 400 - property 'ex' is missing
	it("POST /currency property 'ex' is missing", (done) => {
		chai.request(domain)
		.post("/currency")
		.type("json")
		.send({
			'alias': 'riyadh',
			'name': 'Saudi Arabian Riyadh'
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	//POST /currency 400 - property 'ex' is NOT an object
	it("POST /currency property 'ex' is NOT an object", (done) => {
		chai.request(domain)
		.post("/currency")
		.type("json")
		.send({
			'alias': 'riyadh',
			'name': 'Saudi Arabian Riyadh',
			'ex': "peso: 0.47, usd: 0.0092, won: 10.93, yuan: 0.065"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	//POST /currency 400 - property 'ex' is empty
	it("POST /currency property 'ex' is empty", (done) => {
		chai.request(domain)
		.post("/currency")
		.type("json")
		.send({
			'alias': 'riyadh',
			'name': 'Saudi Arabian Riyadh',
			'ex': ''
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	// //POST /currency 400 - property "alias" is missing
	it("POST /currency property 'alias' is missing", (done) => {
		chai.request(domain)
		.post("/currency")
		.type("json")
		.send({
			'name': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	//POST /currency 400 - property 'alias' is is NOT a string
	it("POST /currency property 'alias' is is NOT a string", (done) => {
		chai.request(domain)
		.post("/currency")
		.type("json")
		.send({
			'alias': undefined,
			'name': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	//POST /currency 400 - property 'alias' is empty
	it("POST /currency property 'alias' is empty", (done) => {
		chai.request(domain)
		.post("/currency")
		.type("json")
		.send({
			'alias': '',
			'name': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	//POST /currency 400 - all fields are complete but there is duplicate alias
	it("POST /currency all fields are complete but there is duplicate alias", (done) => {
		chai.request(domain)
		.post("/currency")
		.type("json")
		.send({
			'alias': 'peso',
			'name': 'Philippines Peso',
			'ex': {
				'usd': 0.02,

			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	//POST /currency 200 - all fields are complete and there are no duplicates
	it("POST /currency route is running, all required fields are complete and there are no duplicates", (done) => {
		chai.request(domain)
		.post("/currency")
		.type("json")
		.send({
			'alias': 'riyadh',
			'name': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();
		})
	})

})
