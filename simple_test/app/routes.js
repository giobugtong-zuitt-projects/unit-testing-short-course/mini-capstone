const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {
		//create if conditions here to check the currency
		const { body } = req

		if (Object.keys(body).length === 0) {
			//return status 200 if route is running
			return res.status(200).send({
				'Message': 'Route is running'
			});
		}
		if (!body.hasOwnProperty("name")) {
			return res.status(400).send({
				"Error": "Bad request - property 'name' is missing"
			});
		}
		
		if (typeof(body.name) !== "string") {
			return res.status(400).send({
				"Error": "Bad request - property 'name' is NOT a string"
			});
		}
		
		if (body.name === "") {
			return res.status(400).send({
				"Error": "Bad request - property 'name' is empty"
			});
		}
		
		if (!body.hasOwnProperty("ex")) {
			return res.status(400).send({
				"Error": "Bad request - property 'ex' is missing"
			});
		}
		
		if (typeof(body.ex) !== "object") {
			return res.status(400).send({
				"Error": "Bad request - property 'ex' is NOT an object"
			});
		}
		
		if (body.ex === "") {
			return res.status(400).send({
				"Error": "Bad request - property 'ex' is empty"
			});
		}
		
		if (!body.hasOwnProperty("alias")) {
			return res.status(400).send({
				"Error": "Bad request - property 'alias' is missing"
			});
		}
		
		if (typeof(body.alias) !== "string") {
			return res.status(400).send({
				"Error": "Bad request - property 'alias' is NOT a string"
			});
		}
		
		if (body.alias === "") {
			return res.status(400).send({
				"Error": "Bad request - property 'alias' is empty"
			});
		}
		
		if (!body.name) {
			return res.status(400).send({
				"Error": "Bad request - property 'name' is missing"
			});
		}
		
		if (exchangeRates[body.alias.toLowerCase()]) {
			return res.status(400).send({
				"Error": `Bad request - ${body.alias.toLowerCase()} already exists!`
			});
		}
		
		if (!exchangeRates[body.alias.toLowerCase()] && body.hasOwnProperty("ex") && body.hasOwnProperty("name")) {
			return res.status(200).send({
				"Message": `Success! ${body.name} has been added.`
			});
		}

	});
}
